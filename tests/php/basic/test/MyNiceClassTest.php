<?php

namespace NiceshopsDev\NiceAcademy\Tests\Basic;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MyNiceClassTest extends TestCase {

    /**
     * @var MyNiceClass|MockObject
     */
    protected $object;
    
    
    protected function setUp() {
        $this->object = $this->getMockBuilder(MyNiceClass::class)->disableOriginalConstructor()->getMockForAbstractClass();
    }

    
    /**
     * 3) Klasse erweitern
     */
    public function testGetString() {
        $myNiceClass = new MyNiceClass();
        $this->assertEquals("always be nice", $myNiceClass->result());
    }

    // =====

    /**
     * 4) Interface implementieren
     */
    public function testInterface() {
        $this->assertEquals(14, (new MyNiceClass())->count());
    }
}