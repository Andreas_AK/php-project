<?php

namespace NiceshopsDev\NiceAcademy\Tests\Basic;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class NiceClassTest extends TestCase
{
    
    
    /**
     * @var NiceClass|MockObject
     */
    protected $object;
    
    
    protected function setUp()
    {
        $this->object = $this->getMockBuilder(NiceClass::class)->disableOriginalConstructor()->getMockForAbstractClass();
    }
    
    
    /**
     * @group  integration
     * @small
     */
    public function testTestClassExists()
    {
        $this->assertTrue(class_exists(NiceClass::class));
        $this->assertTrue($this->object instanceof NiceClass);
    }
 
    // =====

    /**
     * 1) UnitTest schreiben
     * 5) Methoden Sichtbarkeit ändern
     */
    public function testGetString() {
        // 1) Unit Test schreiben
        // $this->assertEquals("be ", NiceClass::getString());


        // 5) Methoden Sichtbarkeit ändern
        $getStringMethod = new \ReflectionMethod($this->object, 'getString');
        $getStringMethod->setAccessible(true);
        $this->assertEquals("be ", $getStringMethod->invoke($this->object));
    }

    // =====

    /**
     * 2) UnitTest mit Data Provider schreiben
     * 
     * @dataProvider provider
     */
    public function testResult($a) {
        $niceClass = new NiceClass();
        $this->assertEquals($a, $niceClass->result());
    }

    /**
     * Provider
     */
    public function provider() {
        return array(
            array("be nice")
        );
    }

    // =====

    /**
     * 4) Interface implementieren
     */
    public function testInterface() {
        $this->assertEquals(7, (new NiceClass())->count());
    }
}
