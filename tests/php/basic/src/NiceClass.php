<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Basic;


class NiceClass implements \Countable
{
    
    
    /**
     * @return string
     */
    private function getString(): string
    {
        return "be ";
    }
    
    
    /**
     * @return string
     */
    public function result(): string
    {
        return trim($this->getString()) . " nice";
    }

    // =====

    /**
     * 4) Interface implementieren
     * 
     * @return int
     */
    public function count() {
        return strlen($this->result());
    }
}