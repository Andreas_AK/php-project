<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Basic;


class MyNiceClass extends NiceClass {

    /**
     * 3) Klasse erweitern
     * 
     * @return string
     */
    public function result(): string {
        return "always " . parent::result();
    }
}