<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Advanced\Shop;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    
    
    /**
     * @var Product|MockObject
     */
    protected $object;
    
    
    protected function setUp()
    {
        $this->object = $this->getMockBuilder(Product::class)->disableOriginalConstructor()->getMockForAbstractClass();
    }
    
    
    /**
     * @group  integration
     * @small
     */
    public function testTestClassExists()
    {
        $this->assertTrue(class_exists(Product::class));
        $this->assertTrue($this->object instanceof Product);
    }

    public function testProductToString() {
        $prod1 = new Product('Prod1', 'Test', new PriceItem(5.985));
        $this->assertEquals("#Prod1 Test, EUR 5,99", strval($prod1));

        $prod2 = new Product('Prod2', 'Test', new PriceItem(5.984));
        $this->assertEquals("#Prod2 Test, EUR 5,98", strval($prod2));
    }
}
