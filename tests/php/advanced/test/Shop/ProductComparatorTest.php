<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Advanced\Shop;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ProductComparatorTest extends TestCase
{
    
    
    /**
     * @var ProductComparator|MockObject
     */
    protected $object;
    
    
    protected function setUp()
    {
        $this->object = $this->getMockBuilder(ProductComparator::class)->disableOriginalConstructor()->getMockForAbstractClass();
    }
    
    
    /**
     * @group  integration
     * @small
     */
    public function testTestClassExists()
    {
        $this->assertTrue(class_exists(ProductComparator::class));
        $this->assertTrue($this->object instanceof ProductComparator);
    }
    
    public function testProductNrTest() {
        $prod1 = new Product('Prod1', 'Test', new PriceItem(5.985));
        $prod2 = new Product('Prod1', 'Test', new PriceItem(5.984));
        $prod3 = new Product('Prod2', 'Test', new PriceItem(5.983));

        $this->assertTrue((new ProductComparator($prod1))->hasSameNumber($prod1));
        $this->assertTrue((new ProductComparator($prod1))->hasSameNumber($prod2));
        $this->assertFalse((new ProductComparator($prod1))->hasSameNumber($prod3));
    }
}
