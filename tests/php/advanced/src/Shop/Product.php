<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Advanced\Shop;


class Product
{
    
    
    /**
     * @var string
     */
    private $number;
    
    
    /**
     * @var string
     */
    private $title = "";
    
    
    /**
     * @var PriceItem
     */
    private $price;
    
    
    /**
     * Product constructor.
     *
     * @param string    $number
     * @param string    $title
     * @param PriceItem $price
     */
    public function __construct(string $number, string $title, PriceItem $price)
    {
        $this->number = $number;
        $this->title = $title;
        $this->price = $price;
    }
    
    
    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }
    
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    
    public function getPrice()
    {
        return $this->price->getPrice();
    }
    
    
    /**
     * @param Product $product
     *
     * @return bool
     */
    /*
    public function hasSameNumber(Product $product): bool
    {
        return $comparator->hasSameNumber($product);
    }
    */
    
    
    /**
     * @return string
     */
    public function __toString()
    {
        return "#" . $this->getNumber() . " " . $this->getTitle() . ", EUR " . number_format($this->getPrice(), 2, ',', ' ');
    }
}