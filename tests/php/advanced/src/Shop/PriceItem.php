<?php
declare(strict_types=1);

namespace NiceshopsDev\NiceAcademy\Tests\Advanced\Shop;


class PriceItem
{
    /**
     * @var float
     */
    private $price = 0.0;
    
    
    /**
     * PriceItem constructor.
     *
     * @param float $price
     */
    public function __construct(float $price)
    {
        $this->price = $price;
    }


    /**
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }
    
    
    /**
     * @param float $price
     *
     * @return $this
     */
    public function addPrice_by_Value(float $price)
    {
        $this->price += floatval($price);
        
        return $this;
    }
    
    
    /**
     * @param PriceItem $priceItem
     *
     * @return $this
     */
    public function addPrice(PriceItem $priceItem)
    {
        $this->price += floatval($priceItem->price);
        
        return $this;
    }
}